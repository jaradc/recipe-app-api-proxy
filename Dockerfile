FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="Jarad"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

# switch to the root user
USER root

# create new directory
RUN mkdir -p /vol/static
# set permissions to 755 so nginx user can read the files
RUN chmod 755 /vol/static
# create an empty file in this location
RUN touch /etc/nginx/conf.d/default.conf
# change ownership of this file to the user nginx and the group nginx (signified by nginx:nginx -> user:group)
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh

# make entrypoint.sh file an executable
RUN chmod +x /entrypoint.sh

USER nginx

CMD ["/entrypoint.sh"]
